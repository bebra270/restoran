# Ресторан
Приложение Ресторан Урманчеева А.О.

Описание предметной области

Ресторан — предприятие общественного питания с широким ассортиментом блюд сложного приготовления, включая заказные и фирменные.

ТЗ

https://gitlab.com/bebra270/restoran/-/blob/main/%D0%A2%D0%97_%D0%A0%D0%B5%D1%81%D1%82%D0%BE%D1%80%D0%B0%D0%BD_%D0%A3%D1%80%D0%BC%D0%B0%D0%BD%D1%87%D0%B5%D0%B5%D0%B2%D0%B0_4%D0%98%D0%A1%D0%9F9-13.docx

ERD модель

![Alt text](image.png)

USE-CASE диаграмма

![Alt text](image-1.png)

Физическая диаграмма БД

![Alt text](image-2.png)

Дизайн в Figma

https://www.figma.com/file/SsVi9QCQFttWw8WL5TCEUy/Restoran?type=design&node-id=0%3A1&mode=design&t=smIUP2tuo7XJk8bG-1

Окна, страницы и классы в приложении

![Alt text](image-5.png)

AppData Class

![Alt text](image-12.png)

Функция скидки

![Alt text](image-6.png)

Окно авторизации, главное окно и заказы

![Alt text](image-13.png)
![Alt text](image-14.png)
![Alt text](image-15.png)


UnitTest

![Alt text](image-7.png)
![Alt text](image-8.png)
![Alt text](image-9.png)
![Alt text](image-10.png)
![Alt text](image-11.png)